# 自作ライブラリをコンポーザーで管理するデモ

## サンプル

プロジェクト名は「composer-test」とする。

composer-test/composer.json に下記を貼り付け。
```json
{
    "name": "vendor/composer-test",
    "description": "composer test",
    "authors": [
        {
            "name": "vendor",
            "email": "your@email.here"
        }
    ],
    "repositories": [
        {
            "type": "git",
            "url": "https://kawamurayuto@bitbucket.org/kawamurayuto/composer-lib-demo.git"
        }
    ],
    "require": {
        "kawamurayuto/composer-lib-demo": "0.0.*"
    }
}
```

composer install し、完了したら
composer-test/index.php に下記を貼り付けて実行

```php
require_once './vendor/autoload.php';

$sample = new Kawamurayuto\ComposerLibDemo\Sample();
$sample->say(); // echo class name.
```

クラス名 ```Kawamurayuto\ComposerLibDemo\Sample``` が出力されたら成功！